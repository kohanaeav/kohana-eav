-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 15 ное 2014 в 21:40
-- Версия на сървъра: 5.5.40-0ubuntu1
-- PHP Version: 5.5.12-2ubuntu4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Структура на таблица `eav_attributes`
--

CREATE TABLE IF NOT EXISTS `eav_attributes` (
`id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` text,
  `default` varchar(255) DEFAULT NULL,
  `validation` varchar(255) DEFAULT NULL,
  `eav_attribute_type_id` int(11) NOT NULL,
  `eav_set_id` int(11) DEFAULT NULL,
  `history` enum('yes','no') NOT NULL DEFAULT 'no',
  `multilanguage` enum('yes','no') NOT NULL DEFAULT 'no',
  `obligatory` enum('yes','no') NOT NULL DEFAULT 'no',
  `unique` enum('yes','no') NOT NULL DEFAULT 'no',
  `multiple` enum('yes','no') NOT NULL DEFAULT 'no',
  `searchable` enum('yes','no') NOT NULL DEFAULT 'no',
  `use_in_listing` enum('yes','no') NOT NULL DEFAULT 'no',
  `use_in_view` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_attribute_options`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_options` (
`id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_attribute_translations`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_translations` (
`id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `language` varchar(5) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_attribute_types`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_types` (
`id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `input_type` varchar(32) NOT NULL,
  `input_multiple` enum('na','yes','no') NOT NULL DEFAULT 'na',
  `table_name` varchar(255) NOT NULL,
  `table_type` varchar(32) NOT NULL,
  `table_len_value` varchar(255) NOT NULL,
  `default_value` varchar(255) NOT NULL,
  `can_history` enum('yes','no') NOT NULL DEFAULT 'yes',
  `can_multilanguage` enum('yes','no') NOT NULL DEFAULT 'no',
  `can_required` enum('yes','no') NOT NULL DEFAULT 'no',
  `can_unique` enum('yes','no') NOT NULL DEFAULT 'no',
  `can_multiple` enum('yes','no') NOT NULL DEFAULT 'no',
  `can_searchable` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_attribute_type_translations`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_type_translations` (
`id` int(11) NOT NULL,
  `eav_attribute_type_id` int(11) NOT NULL,
  `language` varchar(5) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entities`
--

CREATE TABLE IF NOT EXISTS `eav_entities` (
`entity_id` int(11) NOT NULL,
  `entity_guid` varchar(36) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `entity_name` varchar(255) NOT NULL,
  `entity_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entity_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_options`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_options` (
`id` int(11) NOT NULL,
  `eav_entity_attribute_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_entity`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_entity` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `eav_attribute_key` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_entity_values`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_entity_values` (
`id` int(11) NOT NULL,
  `eav_entity_attribute_value_entitiy_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_file`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_file` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `language` char(5) DEFAULT NULL,
  `mime` varchar(32) NOT NULL,
  `size` int(11) NOT NULL,
  `pages` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_file_image_sizes`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_file_image_sizes` (
`id` int(11) NOT NULL,
  `eav_entity_attribute_value_file_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_file_video_formats`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_file_video_formats` (
`id` int(11) NOT NULL,
  `eav_entity_attribute_value_file_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `mime` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_line`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_line` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `eav_attribute_key` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `language` char(5) DEFAULT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_number`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_number` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_point`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_point` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `point` point NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_price`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_price` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_searchable`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_searchable` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `eav_attribute_key` varchar(64) NOT NULL,
  `eav_entity_attribute_value_entity_id` int(11) NOT NULL,
  `language` char(5) DEFAULT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_secret`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_secret` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_select`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_select` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `language` char(5) DEFAULT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_select_values`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_select_values` (
`id` int(11) NOT NULL,
  `eav_entity_attribute_value_select_id` int(11) NOT NULL,
  `eav_entity_attribute_option_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_text`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_text` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `language` char(5) DEFAULT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_timestamp`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_timestamp` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `value` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_attribute_value_yesno`
--

CREATE TABLE IF NOT EXISTS `eav_entity_attribute_value_yesno` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `value` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_entity_translations`
--

CREATE TABLE IF NOT EXISTS `eav_entity_translations` (
`id` int(11) NOT NULL,
  `eav_entity_id` int(11) NOT NULL,
  `language` varchar(5) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_sets`
--

CREATE TABLE IF NOT EXISTS `eav_sets` (
`id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `name` varchar(64) NOT NULL,
  `comment` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Структура на таблица `eav_set_attributes`
--

CREATE TABLE IF NOT EXISTS `eav_set_attributes` (
`id` int(11) NOT NULL,
  `eav_set_id` int(11) NOT NULL,
  `eav_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eav_attributes`
--
ALTER TABLE `eav_attributes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `eav_attribute_options`
--
ALTER TABLE `eav_attribute_options`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_attribute_translations`
--
ALTER TABLE `eav_attribute_translations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_attribute_types`
--
ALTER TABLE `eav_attribute_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_attribute_type_translations`
--
ALTER TABLE `eav_attribute_type_translations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entities`
--
ALTER TABLE `eav_entities`
 ADD PRIMARY KEY (`entity_id`), ADD UNIQUE KEY `entity_guid` (`entity_guid`);

--
-- Indexes for table `eav_entity_attribute_options`
--
ALTER TABLE `eav_entity_attribute_options`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_entity`
--
ALTER TABLE `eav_entity_attribute_value_entity`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_entity_values`
--
ALTER TABLE `eav_entity_attribute_value_entity_values`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `eav_entity_attribute_value_entitiy_id` (`eav_entity_attribute_value_entitiy_id`,`eav_entity_id`);

--
-- Indexes for table `eav_entity_attribute_value_file`
--
ALTER TABLE `eav_entity_attribute_value_file`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_file_image_sizes`
--
ALTER TABLE `eav_entity_attribute_value_file_image_sizes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_file_video_formats`
--
ALTER TABLE `eav_entity_attribute_value_file_video_formats`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_line`
--
ALTER TABLE `eav_entity_attribute_value_line`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_number`
--
ALTER TABLE `eav_entity_attribute_value_number`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_point`
--
ALTER TABLE `eav_entity_attribute_value_point`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_price`
--
ALTER TABLE `eav_entity_attribute_value_price`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_searchable`
--
ALTER TABLE `eav_entity_attribute_value_searchable`
 ADD PRIMARY KEY (`id`), ADD FULLTEXT KEY `value` (`value`);

--
-- Indexes for table `eav_entity_attribute_value_secret`
--
ALTER TABLE `eav_entity_attribute_value_secret`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_select`
--
ALTER TABLE `eav_entity_attribute_value_select`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_select_values`
--
ALTER TABLE `eav_entity_attribute_value_select_values`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `eav_entity_attribute_value_option_id` (`eav_entity_attribute_value_select_id`,`eav_entity_attribute_option_id`);

--
-- Indexes for table `eav_entity_attribute_value_text`
--
ALTER TABLE `eav_entity_attribute_value_text`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_timestamp`
--
ALTER TABLE `eav_entity_attribute_value_timestamp`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_attribute_value_yesno`
--
ALTER TABLE `eav_entity_attribute_value_yesno`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_entity_translations`
--
ALTER TABLE `eav_entity_translations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eav_sets`
--
ALTER TABLE `eav_sets`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `eav_set_attributes`
--
ALTER TABLE `eav_set_attributes`
 ADD UNIQUE KEY `id` (`id`), ADD UNIQUE KEY `eav_entity_id` (`eav_set_id`,`eav_attribute_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `eav_attributes`
--
ALTER TABLE `eav_attributes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `eav_attribute_options`
--
ALTER TABLE `eav_attribute_options`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_attribute_translations`
--
ALTER TABLE `eav_attribute_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_attribute_types`
--
ALTER TABLE `eav_attribute_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `eav_attribute_type_translations`
--
ALTER TABLE `eav_attribute_type_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entities`
--
ALTER TABLE `eav_entities`
MODIFY `entity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_options`
--
ALTER TABLE `eav_entity_attribute_options`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_entity`
--
ALTER TABLE `eav_entity_attribute_value_entity`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_entity_values`
--
ALTER TABLE `eav_entity_attribute_value_entity_values`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_file`
--
ALTER TABLE `eav_entity_attribute_value_file`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_file_image_sizes`
--
ALTER TABLE `eav_entity_attribute_value_file_image_sizes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_file_video_formats`
--
ALTER TABLE `eav_entity_attribute_value_file_video_formats`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_line`
--
ALTER TABLE `eav_entity_attribute_value_line`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_number`
--
ALTER TABLE `eav_entity_attribute_value_number`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_point`
--
ALTER TABLE `eav_entity_attribute_value_point`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_price`
--
ALTER TABLE `eav_entity_attribute_value_price`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_searchable`
--
ALTER TABLE `eav_entity_attribute_value_searchable`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_secret`
--
ALTER TABLE `eav_entity_attribute_value_secret`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_select`
--
ALTER TABLE `eav_entity_attribute_value_select`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_select_values`
--
ALTER TABLE `eav_entity_attribute_value_select_values`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_text`
--
ALTER TABLE `eav_entity_attribute_value_text`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_timestamp`
--
ALTER TABLE `eav_entity_attribute_value_timestamp`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_attribute_value_yesno`
--
ALTER TABLE `eav_entity_attribute_value_yesno`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_entity_translations`
--
ALTER TABLE `eav_entity_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `eav_sets`
--
ALTER TABLE `eav_sets`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `eav_set_attributes`
--
ALTER TABLE `eav_set_attributes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
