<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Default EAV Attributes
 *
 * @author Petar
 * Created on 2014-3-25
 */
class Kohana_EAV_Attribute extends ORM {

	protected $_belongs_to = array(
		'type' => array(
			'model' => 'EAV_Attribute_Type',
			'foreign_key' => 'eav_attribute_type_id'
		),
	);
	protected $_has_many = array(
		'options' => array(
			'model' => 'EAV_Attribute_Option',
		),
		'sets' => array(
			'model' => 'EAV_Set',
			'through' => 'eav_set_attributes',
			'far_key' => 'eav_set_id',
			'foreign_key' => 'eav_attribute_id',
		),
	);

	public function rules()
	{
		return array(
//			'name' => array(
//				array('not_empty'),
//				array('max_length', array(':value', 32)),
//				array('regex', array(':value', '/^[a-z0-9_]*$/')),
//				array(array($this, 'unique_name'), array(':value')),
//			),
//			'eav_attribute_type_id' => array(array('not_empty')),
//			'obligatory' => array(
//				array('regex', array(':value', '(no|yes)'))
//			),
//			'unique' => array(
//				array('regex', array(':value', '(no|yes)'))
//			),
//			'validation' => array(
//				array('max_length', array(':value', 255))
//			),
		);
	}

	public function unique_name($name)
	{
		$result = ORM::factory('EAV_Attribute')->where('name', '=', $name);
		if ($this->loaded())
		{
			$result = $result->and_where('id', '<>', $this->pk());
		}
		return ! $result->find()->loaded();
	}

	public function filters()
	{
		return array(
			'name' => array(
				array('trim'),
			),
			'validation' => array(
				array('trim'),
			),
			'comment' => array(
				array(function($value)
					{
						return ($value === '') ? NULL : $value;
					})
			),
		);
	}

//	public function checkboxes()
//	{
//		return array(
//			'obligatory',
//			'unique',
//			'use_in_company',
//			'use_in_documents',
//			'use_in_document_articles',
//			'use_in_tax_rules',
//			'use_in_account_pairs',
//			'use_in_reporting',
//			'use_in_sorting',
//			'show_in_listing',
//			'show_in_view',
//			'use_in_document_rules',
//			'use_in_totaling_rules',
//			'use_in_accounting_rules',
//		);
//	}

	public function is_object()
	{
		return ($this->loaded() AND $this->type->value_type === 'object');
	}

	public function attr_set()
	{
		if ( ! $this->loaded() OR $this->type->value_type !== 'object')
			return NULL;


		return ORM::factory($this->type->class . '_Set', $this->set_id);
	}

	public function expected()
	{
		$edit = array(
			'name',
			'comment',
			'default',
			'validation',
			'history',
//			'multilanguage',
			'obligatory',
			'unique',
			'searchable',
			'use_in_listing',
			'use_in_view',
			'modified',
		);

		$add = array_merge($edit, array(
			'key',
			'eav_attribute_type_id',
			'multilanguage',
		));
		$key = array_search("modified", $add);
		unset($add[$key]);

		return $this->loaded() ? $edit : $add;
	}

	public function to_ajax($data = array())
	{
		if ($this->loaded())
		{
			foreach ($this->_object as $key => $value)
			{
				$data[$key] = $value;
			}
		}
		return $data;
	}

}

?>
