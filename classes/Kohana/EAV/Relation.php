<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Default EAV Attributes
 *
 * @author Petar
 * Created on 2014-3-25
 */
class Kohana_EAV_Relation extends ORM {

	protected $_belongs_to = array(
		'set' => array(
			'model' => 'EAV_Set',
			'foreign_key' => 'eav_set_id'
		),
	);
	
	protected $_table_name = "eav_relations";

	public function __construct($set = NULL)
	{
		$this->_initialize();
		
		if ($set !== NULL)
		{
			$this->where("eav_set_id", '=', $set->id);
		}
		elseif ( ! empty($this->_cast_data))
		{
			// Load preloaded data from a database call cast
			$this->_load_values($this->_cast_data);

			$this->_cast_data = array();
		}
	}

	public static function factory($model, $set = null)
	{
		if ( ! is_object($set))
		{
			$set = EAV::factory("Set")->where("key", "=", strtolower($set))->find();
		}
		$eavmodel = 'Model_EAV_Relation_' . $model;

		if ($eavmodel != "Model_EAV_Relation_")
		{
			if ( ! class_exists($eavmodel, true))
			{
				return new $eavmodel($set);
			}
			else
			{
				var_dump($model);
				var_dump($id);
			}
		}
		else
		{
			var_dump($model);
			var_dump($id);
		}
	}

}

?>
