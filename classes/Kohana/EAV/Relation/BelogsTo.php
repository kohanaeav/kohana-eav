<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Default EAV Attributes
 *
 * @author Petar
 * Created on 2014-3-25
 */
class Kohana_EAV_Relation_BelongsTo extends Kohana_EAV_Relation {

	protected $_has_one = array(
		'sets' => array(
			'model' => 'EAV_Set',
			'foreign_key' => 'eav_set_id'
		),
	);

}

?>
