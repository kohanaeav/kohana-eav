<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Set_Attribute extends ORM {
	
	protected $_table_name = "eav_set_attributes";

	public $_belongs_to = array(
		'attribute' => array(
			'model' => 'EAV_Attribute',
			'foreign_key' => 'eav_attribute_id',
		),
		'set' => array(
			'model' => 'EAV_Entity_Set',
			'foreign_key' => 'eav_set_id',
		),
	);

	public function rules()
	{

		return array(
			'eav_attribute_id' => array(
				array('not_empty'),
			),
			'eav_set_id' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
//			'has_many' => array(
//				array('Form::format_checkbox'),
//			),
//			'start_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
//			'end_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
		);
	}

}
