<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Set_Relation extends ORM {
	
	protected $_table_name = "eav_set_relations";

	public $_belongs_to = array(
		'up_set' => array(
			'model' => 'EAV_Entity_Set',
			'foreign_key' => 'eav_up_set_id',
		),
		'down_set' => array(
			'model' => 'EAV_Entity_Set',
			'foreign_key' => 'eav_down_set_id',
		),
	);
	
	public static function getRelations($set) {
		$id = $set->pk();
		return eav::factory("Set_Relation")->
				where("eav_up_set_id", "=", $id)->
				or_where("eav_down_set_id", "=", $id)->
				find_all();
	}

	public function rules()
	{

		return array(
			'eav_up_set_id' => array(
				array('not_empty'),
			),
			'eav_down_set_id' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
//			'has_many' => array(
//				array('Form::format_checkbox'),
//			),
//			'start_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
//			'end_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
		);
	}

}
