<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Kohana_EAV_Entity extends ORM {

	public $_belongs_to = array(
		'set' => array(
			'model' => 'EAV_Set',
			'foreign_key' => 'eav_set_id',
		)
	);
	protected $_primary_key = 'entity_id';
	protected $_model = 'null';
	protected $_table_name = "eav_entities";
	protected $_entity_attributes_values = array();

	public static function factory($set, $id = NULL)
	{
		if ( ! is_object($set))
		{
			$set = EAV::factory("Set")->where("key", "=", strtolower($set))->find();
		}
		if ($set->loaded())
		{
			return new Model_EAV_Entity($set, $id);
		}
		else
		{
			DataX::vardump($set);
			DataX::vardump($id);
			throw new EAV_Exception_Entity_Notfound();
		}
	}

	public function __construct($set = null, $id = NULL)
	{
		parent::__construct($id);
		if ( ! $this->loaded())
		{
			if (is_object($set))
			{
				$this->_model = $set->key;
				$this->and_where("eav_entity.eav_set_id", "=", $set->id);
				$this->_related["set"] = $set;
				$attributes = $set->attributes->find_all();
				foreach ($attributes as $attribute)
				{
					$this->_entity_attributes_values[$attribute->key] = Model_EAV_Entity_Attribute_Value::value_factory($attribute->type->name, $this->set, $this, $attribute);
				}
			}
		}
		else
		{
			$this->_initialize_entity_attributes_values();
		}
	}

	public function find()
	{
		parent::find();
		if ($this->_loaded)
		{
			return $this->_initialize_entity_attributes_values();
		}
		return $this;
	}

	public function reload()
	{
		parent::reload();
		if ($this->_loaded)
		{
			return $this->_initialize_entity_attributes_values();
		}
		return $this;
	}

//	public function find()
//	{
//		$find = parent::find();
////		if ($this->loaded())
////		{
////			$this->_initialize_entity_attributes_values();
////		}
//		return $find;
//	}

	protected function _initialize_entity_attributes_values()
	{
//		if (
//				($this->_entity_attributes_values === NULL) or (
//				is_array($this->_entity_attributes_values) and ( count($this->_entity_attributes_values) == 0)
//				)
//		)
//		{
		$this->_entity_attributes_values = array();
		$set = $this->get("set");
		$attributes = $set->attributes->find_all();
		foreach ($attributes as $attribute)
		{
			$this->_entity_attributes_values[$attribute->key] = Model_EAV_Entity_Attribute_Value::value_factory($attribute->type->name, $this->set, $this, $attribute);
		}
		$related_sets = $set->get_relations();
//		foreach ($related_sets as $related_set)
//		{
//			$this->_entity_related_values[$related_set->key] = Model_EAV_Entity_Attribute_Value::value_factory($attribute->type->name, $this->set, $this, $attribute);
//		}
//		}
		return $this;
	}

	public function get($column, $type = "object")
	{
		if (array_key_exists($column, $this->_entity_attributes_values))
		{
			if ($type == "object")
			{
				return $this->_entity_attributes_values[$column];
			}
			else
			{
				return $this->_entity_attributes_values[$column]->get_value();
			}
		}
		return parent::get($column);
	}

	public function set($column, $value)
	{

		if (array_key_exists($column, $this->_entity_attributes_values))
		{
			if (is_object($value))
			{
				$this->_entity_attributes_values[$column] = $value;
			}
			else
			{
				if ($this->_entity_attributes_values[$column]->loaded())
				{
					$this->_entity_attributes_values[$column]->set_value($value);
				}
				else
				{
					$attribute = $this->set->attributes->where("key", "=", $column)->find();
					$this->_entity_attributes_values[$column] = Kohana_EAV_Entity_Attribute_Value::createnew($attribute->type->name, $this->set, $this, $attribute, $value);
				}
			}
			return $this;
		}

		return parent::set($column, $value);
	}

	public function where($column, $op, $value)
	{
		if (array_key_exists($column, $this->_entity_attributes_values))
		{
			$table = $this->_entity_attributes_values[$column]->table_name();
			$this->join($table)
					->on("$table.eav_entity_id", "=", "entity_id")
					->where("$table.value", $op, $value)
					->and_where("$table.eav_attribute_key", "=", $column);
		}
		else
		{
			return parent::where($column, $op, $value);
		}

		return $this;
	}

	public function and_where($column, $op, $value)
	{
		if (array_key_exists($column, $this->_entity_attributes_values))
		{
			$table = $this->_entity_attributes_values[$column]->table_name();
			$this->join($table)
					->on("$table.eav_entity_id", "=", "entity_id")
					->and_where("$table.value", $op, $value)
					->and_where("$table.eav_attribute_key", "=", $column);
		}
		else
		{
			return parent::and_where($column, $op, $value);
		}

		return $this;
	}

	public function get_name()
	{
		return $this->get("entity_name");
	}

	public function get_url($params = NULL)
	{
		if (array_key_exists("slug", $this->_entity_attributes_values))
		{
			return "/{$this->_entity_attributes_values["slug"]->get_value()}" . ( ! is_null($params) ? $params : "");
		}
		return "/{$this->set->key}/{$this->entity_guid}" . ( ! is_null($params) ? $params : "");
	}

	public function check_value($column, $value)
	{
		if (array_key_exists($column, $this->_entity_attributes_values))
		{
			return $this->_entity_attributes_values[$column]->check_value($value);
		}
		else
		{
			return ($this->_object[$column] != $value);
		}
	}

}
