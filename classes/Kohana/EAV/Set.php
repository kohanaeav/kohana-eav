<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model Set
 *
 * @author Petar
 * Created on 2014-3-25
 */
class Kohana_EAV_Set extends ORM {

	public $_has_many = array(
		'attributes' => array(
			'model' => 'EAV_Attribute',
			'through' => 'eav_set_attributes',
			'far_key' => 'eav_attribute_id',
			'foreign_key' => 'eav_set_id',
		),
		'related' => array(
			'model' => 'EAV_Set',
			'through' => 'eav_set_relations',
			'far_key' => 'eav_down_set_id',
			'foreign_key' => 'eav_up_set_id',
		),
		'set_attributes' => array(
			'model' => 'EAV_Set_Attribute',
		),
		'set_relations' => array(
			'model' => 'EAV_Set_Relation',
		),
		'entities' => array(
			'model' => 'EAV_Entity',
			'foreign_key' => 'eav_set_id',
		)
	);
	protected $_visual_dataset = array();
	
	/**
	 * "Has one" eav relationships
	 * @var array
	 */
	protected $_relation_has_one = array();

	/**
	 * "Belongs to" eav relationships
	 * @var array
	 */
	protected $_relation_belongs_to = array();

	/**
	 * "Has many" eav relationships
	 * @var array
	 */
	protected $_relation_has_many = array();
	
	protected function _initialize()
	{
		// Set the object name if none predefined
//		if (empty($this->_object_name))
//		{
//			$this->_object_name = strtolower(substr(get_class($this), 6));
//		}
//		if ( ! $init = Arr::get(ORM::$_init_cache, $this->_object_name, FALSE))
//		{
//			
//			
//			$relations_belongs_to = new EAV_Relation($this);
//			$relations_belongs_to->find_all();
//			if (
//					((is_object($relations_belongs_to)) and (get_class($relations_belongs_to)=="EAV_Relation") and ($relations_belongs_to->loaded())
//					) or ((is_object($relations_belongs_to)) and (get_class($relations_belongs_to)=="DB") and (count($relations_belongs_to)>0))
//					)
//			{
//				foreach ($relations_belongs_to as $relation_belongs_to) {
//					DataX::vardump($relation_belongs_to);
//					$var = "_relation_{$relation_belongs_to->type}";
//					$this->$var->type[$relation_belongs_to->key] = $relation_belongs_to->eav_related_set_id;
//				}
//			}
//			
//			
//		}
		parent::_initialize();
	}

	public function rules()
	{
		return array(
//			'name' => array(
//				array('not_empty'),
//				array('max_length', array(':value', 45)),
//				array('regex', array(':value', '/^[a-z0-9_]*$/')),
//				array(array($this, 'unique_name'), array(':value')),
//			),
		);
	}

	public function filters()
	{
		return array(
			'name' => array(
				array('trim'),
			),
		);
	}

	public function create_set($data)
	{
		$expected = array('key', 'name', 'comment');

		if ($data['comment'] === '')
		{
			$data['comment'] = NULL;
		}

		$this->values($data, $expected);
		$this->save();
	}

	public function update_set($data)
	{
		$expected = array('name', 'comment');
		if ($data['comment'] === '')
		{
			$data['comment'] = NULL;
		}
		if (($this->name_attribute_id == 0) and ( $data["name_attribute"] > 0))
		{
			$set_name_attribute = $this->attributes->where("eav_attribute.id", "=", $data["name_attribute"])->find();
			$this->set("name_attribute_id", $set_name_attribute->id);
			$this->set("name_attribute_key", $set_name_attribute->key);
			unset($data["name_attribute"]);
		}
		$this->values($data, $expected);
		$this->save();
	}

	/**
	 * Delete set if there arent any child sets, rules, or rows associated with this set
	 * @return bool on success true else false
	 */
	public function delete_set()
	{
		//name of the set to be deleted
		$set_name = EAV_Helper::ORM_name($this->_table_name);
		//load all EAV objects names
		$eavs = ORM::factory('EAV_Attribute_Type')->where('value_type', '=', 'object')->find_all();
		$user_eavs_objects = ORM::factory('User_Eav')->find_all();
		$user_eavs = array();
		$all_credentials = ORM::factory('EAV_Company_Credential')->find_all();
		foreach ($user_eavs_objects as $user_eav)
		{
			array_push($user_eavs, $user_eav->class);
		}
		//check if there are any eav_objects associated with this set
		foreach ($eavs as $eav)
		{
			if (in_array($eav->class, $user_eavs))
			{
				//check for default_values in our db
				$object = EAV::factory($eav->class . '_Default');
				if ($object->_belongs_to['set'] ['model'] == $set_name)
				{
					if ($this->has_rules($eav->class . '_Default'))
						return FALSE;

					$object_rows = $object->where($object->_belongs_to['set']['foreign_key'], '=', $this->id)->find();

					if ($object_rows->loaded())
					{
						return FALSE;
					}
				}
				//$object->clear_cache();
				// walk all users dbs...		
				foreach ($all_credentials as $credentials)
				{
					//$this->session->set('company', $credentials->eav_company_id);
					Session::instance()->set('company', $credentials->eav_company_id);
					$object = EAV::factory($eav->class);

					if ($object->_belongs_to['set'] ['model'] == $set_name)
					{
						if ($this->has_rules($eav->class))
							return FALSE;

						$object_rows = $object->where($object->_belongs_to['set']['foreign_key'], '=', $this->id)->find();

						if ($object_rows->loaded())
						{
							return FALSE;
						}
					}
					$object->clear_cache();
				}
			}
			else
			{
				$object = EAV::factory($eav->class);

				if ($object->_belongs_to['set'] ['model'] == $set_name)
				{
					if ($this->has_rules($eav->class))
						return FALSE;

					$object_rows = $object->where($object->_belongs_to['set']['foreign_key'], '=', $this->id)->find();

					if ($object_rows->loaded())
					{
						return FALSE;
					}
				}
			}
		}
		//check if there are any child sets
		$children = EAV::factory($set_name)->where('parent_set_id', '=', $this->id)->find();
		if ($children->loaded())
		{
			return FALSE;
		}

		//actual delete 
		try
		{
			$db = Database::instance();
			$db->begin();
			$this->delete();
			$db->commit();
			return TRUE;
		} catch (Exception $e)
		{
			$db->rollback();
			return FALSE;
		}
	}

	/**
	 * Checks if there is a rules for that set and entity name
	 */
	protected function has_rules($eav_class_name)
	{
		$event_rule = ORM::factory('Event_Rule')
				->join('events')
				->on('events.id', '=', 'event_rule.event_id')
				->where('events.entity', '=', $eav_class_name)
				->and_where('event_rule.set_id', '=', $this->id);
		$event_rule = $event_rule->find();
		if ($event_rule->loaded())
			return TRUE;

		return FALSE;
	}

	/**
	 * Check if the name is unique in set table
	 * @param  string $name name of the set - identifier
	 * @return bool       unique - true else false
	 */
	public function unique_name($name)
	{
		//edit
		if ($this->id !== NULL AND $this->name == $name)
			return TRUE;
		//new
		$result = ORM::factory('EAV_Company_Set')->where('name', '=', $name)->find();
		if ($result->loaded())
			return FALSE;
		else
			return TRUE;
	}

	public function get_set_attributes_table()
	{
		return $this->_has_many['attributes']['through'];
	}

	/**
	 * Add or remove parent set attributes to all his children
	 * @param  EAV_Attribute $attribute attribute
	 * @param  string 		 $action    add | remove
	 * @return bool          
	 */
	public function update_children_attributes($attribute_id, $attribute_options, $action)
	{
		//get all child sets recursevly
		$children = array();
		EAV_Helper::get_set_children($this, $children);

		if ($action === 'add')
		{
			foreach ($children as $child_set_id)
			{
				$eav_set = ORM::factory($this->_object_name, $child_set_id);
				if ( ! $eav_set->has('attributes', $attribute_id))
					$eav_set->add_attr($attribute_id, isset($attribute_options['has_many']) ? $attribute_options['has_many'] : NULL, $attribute_options['start_date'], $attribute_options['end_date']
					);
			}
		}
		elseif ($action === 'remove')
		{
			foreach ($children as $child_set_id)
			{
				$eav_set = ORM::factory($this->_object_name, $child_set_id);
				if ($eav_set->has('attributes', $attribute_id))
				{
					$attribue = ORM::factory('EAV_Attribute', $attribute_id);
					$eav_set->remove('attributes', $attribute);
				}
			}
		}
		elseif ($action === 'update')
		{
			foreach ($children as $child_set_id)
			{
				$eav_set = ORM::factory($this->_object_name, $child_set_id);
				if ($eav_set->has('attributes', $attribute_id))
				{
					$set_attr = $eav_set->set_attributes->where('eav_attribute_id', '=', $attribute_id)->find();
					#$set_attr = ORM::factory($this->_object_name.'_Attribute')->where('eav_attribute_id', '=', $attribute_id)->and_where('', '=', $eav_set->id)->find();
					$set_attr->has_many = isset($attribute_options['has_many']) ? $attribute_options['has_many'] : NULL;
					$set_attr->start_date = $attribute_options['start_date'];
					$set_attr->end_date = $attribute_options['end_date'];
					$set_attr->save();
				}
				else
				{
					$eav_set->add_attr($attribute_id, isset($attribute_options['has_many']) ? $attribute_options['has_many'] : NULL, $attribute_options['start_date'], $attribute_options['end_date']
					);
				}
			}
		}
	}

	/**
	 * add attribute with his options to eav_set
	 * @param int  $attribute_id eav_attribute_id
	 * @param timestamp  $start_date   date to begin
	 * @param timestamp  $end_date     date to end
	 * @param bool 		 $has_many     if true has_many relation is active
	 */
	public function add_attr($attribute_id, $has_many = 0, $start_date = NULL, $end_date = NULL)
	{
		$eav_attribute = ORM::factory('EAV_Attribute', $attribute_id);
		$this->add('attributes', $eav_attribute);
		$last_added_row = $this->set_attributes->where('eav_attribute_id', '=', $attribute_id)->and_where($this->_object_name . '_id', '=', $this->id)->find();
		$last_added_row->has_many = $has_many;
		$last_added_row->start_date = $start_date;
		$last_added_row->end_date = $end_date;
		$last_added_row->save();
	}

	public function set_has($type, $far_keys = NULL)
	{
		$count = $this->eav_count_relations($type, $far_keys);
		if ($far_keys === NULL)
		{
			return (bool) $count;
		}
		else
		{
			return $count === count($far_keys);
		}
	}

	public function set_add($type, $name, $far_keys)
	{
		//far_key = down
		$far_keys = ($far_keys instanceof ORM) ? $far_keys->pk() : $far_keys;

		if (($type == "hm_bt") or ( $type == "ho_bt") or ( $type == "ho_hm"))
		{
			$columns = array("name", $this->_has_many['related']['foreign_key'], "type", $this->_has_many['related']['far_key']);
		}
		else
		{
			if ($type == "bt_hm")
			{
				$type = "hm_bt";
			}
			else if ($type == "bt_ho")
			{
				$type = "ho_bt";
			}
			else if ($type == "hm_ho")
			{
				$type = "ho_hm";
			}
			$columns = array("name", $this->_has_many['related']['far_key'], "type", $this->_has_many['related']['foreign_key']);
		}
		$foreign_key = $this->pk();

		$query = DB::insert($this->_has_many['related']['through'], $columns);

		foreach ((array) $far_keys as $key)
		{
			$query->values(array($name, $foreign_key, $type, $key));
		}

		$query->execute($this->_db);

		return $this;
	}

	public function set_remove($type, $far_keys = NULL)
	{
		$far_keys = ($far_keys instanceof ORM) ? $far_keys->pk() : $far_keys;

		if (($type == "hm_bt") or ( $type == "ho_bt") or ( $type == "ho_hm"))
		{
			DataX::vardump("1");
			$query = DB::delete($this->_has_many['related']['through'])
					->where($this->_has_many['related']['foreign_key'], '=', $this->pk())
					->where("type", '=', $type);
		}
		else
		{
			DataX::vardump("2");
			if ($type == "bt_hm")
			{
				$type = "hm_bt";
			}
			else if ($type == "bt_ho")
			{
				$type = "ho_bt";
			}
			else if ($type == "hm_ho")
			{
				$type = "ho_hm";
			}
			$query = DB::delete($this->_has_many['related']['through'])
					->where($this->_has_many['related']['far_key'], '=', $this->pk())
					->where("type", '=', $type);
		}
		if ($far_keys !== NULL)
		{
			DataX::vardump("3");
			if ($type == "has_mtm")
			{
				DataX::vardump("4");
				$query->where($this->_has_many['related']['foreign_key'], 'IN', (array) $far_keys);
			}
			else
			{
				DataX::vardump("5");
				$query->where($this->_has_many['related']['far_key'], 'IN', (array) $far_keys);
			}
		}

		$query->execute($this->_db);

		return $this;
	}

	public function attribute_has($far_keys = NULL)
	{
		$count = $this->eav_count_relations("atttribute", $far_keys);
		if ($far_keys === NULL)
		{
			return (bool) $count;
		}
		else
		{
			return $count === count($far_keys);
		}
	}

	public function attribute_add($far_keys)
	{
		$far_keys = ($far_keys instanceof ORM) ? $far_keys->pk() : $far_keys;

		$columns = array($this->_has_many['attributes']['foreign_key'], $this->_has_many['attributes']['far_key']);
		$foreign_key = $this->pk();

		$query = DB::insert($this->_has_many['attributes']['through'], $columns);

		foreach ((array) $far_keys as $key)
		{
			$query->values(array($foreign_key, $key));
		}

		$query->execute($this->_db);

		return $this;
	}

	public function attribute_remove($far_keys = NULL)
	{
		$far_keys = ($far_keys instanceof ORM) ? $far_keys->pk() : $far_keys;

		$query = DB::delete($this->_has_many['attributes']['through'])
				->where($this->_has_many['attributes']['foreign_key'], '=', $this->pk());

		if ($far_keys !== NULL)
		{
			// Remove all the relationships in the array
			$query->where($this->_has_many['attributes']['far_key'], 'IN', (array) $far_keys);
		}

		$query->execute($this->_db);

		return $this;
	}

	public function eav_count_relations($type, $far_keys = NULL)
	{
		$key = "related";
		if ($type == "attributes")
		{
			$key = "attributes";
		}

		if ($far_keys === NULL)
		{
			$db = DB::select(array(DB::expr('COUNT(*)'), 'records_found'))
					->from($this->_has_many[$key]['through'])
					->where($this->_has_many[$key]['foreign_key'], '=', $this->pk());
			if ($type != "attributes")
			{
				$db->where("type", '=', $type);
			}
			return (int) $db->execute($this->_db)->get('records_found');
		}

		$far_keys = ($far_keys instanceof ORM) ? $far_keys->pk() : $far_keys;

		// We need an array to simplify the logic
		$far_keys = (array) $far_keys;

		// Nothing to check if the model isn't loaded or we don't have any far_keys
		if ( ! $far_keys OR ! $this->_loaded)
			return 0;

		$db = DB::select(array(DB::expr('COUNT(*)'), 'records_found'))
				->from($this->_has_many[$key]['through'])
				->where($this->_has_many[$key]['foreign_key'], '=', $this->pk());
		if ($type != "attributes")
		{
			$db->where("type", '=', $type);
		}
		$db->where($this->_has_many[$key]['far_key'], 'IN', $far_keys);
		$count = (int) $db->execute($this->_db)->get('records_found');

		// Rows found need to match the rows searched
		return (int) $count;
	}

	public function get_relations()
	{
		$relations = Model_EAV_Set_Relation::getRelations($this);
		$data = array();
		foreach ($relations as $related)
		{
			//DataX::vardump($related);
			$type = $related->type;
			if ($related->eav_down_set_id != $this->id)
			{
				$data[$type][$related->id]['set'] = EAV::factory("Set", $related->eav_down_set_id);
			}
			else
			{
				if ($related->type == "hm_bt")
				{
					$type = "bt_hm";
				}
				else if ($related->type == "ho_bt")
				{
					$type = "bt_ho";
				}
				else if ($related->type == "ho_hm")
				{
					$type = "hm_ho";
				}
				$data[$type][$related->id]['set'] = EAV::factory("Set", $related->eav_up_set_id);
			}
			$data[$type][$related->id]['relation'] = $related;
		}
		return $data;
	}

}

?>
