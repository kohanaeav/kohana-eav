<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value extends ORM {

	public $_belongs_to = array(
		'entity' => array(
			'model' => 'EAV_Entity',
			'foreign_key' => 'eav_entity_id',
		),
		'set' => array(
			'model' => 'EAV_Set',
			'foreign_key' => 'eav_set_id',
		),
		'attribute' => array(
			'model' => 'EAV_Attribute',
			'foreign_key' => 'eav_attribute_id',
		),
	);
	protected $_value_model = "generic";

	public static function value_factory($model, $set, $entity, $attribute = NULL, $id = NULL)
	{
// Set class name
		if ($model == "Date / Time")
		{
			$model = "Datetime";
		}
		$model = 'Model_EAV_Entity_Attribute_Value_' . $model;

		if ($model != "Model_EAV_Entity_Attribute_Value")
		{
			return new $model($set, $entity, $attribute, $id);
		}
		else
		{
			var_dump($model);
			var_dump($id);
		}
	}

	public static function createnew($model, $set, $entity, $attribute, $value)
	{
		$model = 'Model_EAV_Entity_Attribute_Value_' . $model;
		$obj = new $model($set, $entity, $attribute);
		$obj->set("eav_set_id", $set->id);
		$obj->set("eav_entity_id", $entity->entity_id);
		$obj->set("eav_attribute_id", $attribute->id);
		$obj->set("eav_attribute_key", $attribute->key);
		$obj->set("value", $value);
		$obj->save();
		$obj->reload();
		//$obj->set_value($value, true);
		return $obj;
	}

	public function __construct($set, $entity, $attribute, $id = NULL)
	{
		$this->_initialize();
		if ($id !== NULL)
		{
			$this->where($this->_object_name . '.' . $this->_primary_key, '=', $id);
			$this->find();
		}
		else if (($set->id !== NULL) and ( $entity->entity_id !== NULL) and ( $attribute->id !== NULL))
		{
			$this->where("eav_set_id", '=', $set->id);
			$this->and_where("eav_entity_id", '=', $entity->entity_id);
			$this->and_where("eav_attribute_id", '=', $attribute->id);
			$this->order_by("created", "DESC");
			$this->limit(1);
			$this->find();
		}
	}

	public function model()
	{
		return $this->_value_model;
	}

	public function rules()
	{
		return array(
			'eav_attribute_id' => array(
				array('not_empty'),
			),
			'eav_entity_id' => array(
				array('not_empty'),
			),
			'eav_set_id' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
//			'has_many' => array(
//				array('Form::format_checkbox'),
//			),
//			'start_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
//			'end_date' => array(
//				array('trim'),
//				array('Date::to_mysql_timestamp'),
//			),
		);
	}

	public function get_value()
	{
		if ($this->attribute->multiple == "yes")
		{
			switch ($this->attribute->type->name)
			{
				case "Entity":
					$model = "EAV_Entity_Attribute_Value_Entity_Value";
					$value_key = "eav_entity_attribute_value_entitiy_id";
					$option_key = "eav_entity_id";
					break;
				case "Multiple Select":
					$model = "EAV_Entity_Attribute_Value_Select_Value";
					$value_key = "eav_entity_attribute_value_select_id";
					$option_key = "eav_entity_attribute_option_id";
					break;
			}
			$value = ORM::factory($model)->where($value_key, "=", $this->id)->find_all()->as_array($option_key, $option_key);
			if (is_null($value))
			{
				$value = array();
			}
			return $value;
		}
		return $this->get("value");
	}

	public function check_value($new_value)
	{
		$cur_value = $this->get("value");
		if ($this->attribute->multiple == "yes")
		{
			if ( ! $cur_value)
			{
				$cur_value = array();
			}
			if ( ! is_array($new_value))
			{
				$new_value = array();
			}
			sort($cur_value);
			sort($new_value);
		}
		return ($cur_value != $new_value);
	}

	public function set_value($value, $new = false)
	{
		if ($this->check_value($value))
		{
			$class = get_class($this);
			if ($this->attribute->history == "yes" and ! $new)
			{
				$obj = new $class($this->set, $this->entity, $this->attribute);
				$obj->set("eav_set_id", $this->set->id);
				$obj->set("eav_entity_id", $this->entity->entity_id);
				$obj->set("eav_attribute_id", $this->attribute->id);
				$obj->set("eav_attribute_key", $this->attribute->key);
				$obj->set("value", $value);
				$obj->save();
				$obj->reload();
				//$obj->set_value($value, true);
				return $obj;
			}
			else
			{
				if ( ! $this->loaded())
				{
					$this->set("eav_set_id", $this->set->id);
					$this->set("eav_entity_id", $this->entity->entity_id);
					$this->set("eav_attribute_id", $this->attribute->id);
					$this->set("eav_attribute_key", $this->attribute->key);
				}
				else
				{
					if ($this->attribute->unique == "yes")
					{
						$q = DB::select();
						$q->from($this->_table_name);
						$q->where("eav_set_id", "=", $this->set->id);
						$q->and_where("eav_attribute_id", '=', $this->attribute->id);
						$q->and_where("value", "=", $value);
						$r = $q->execute();
						if ((count($r) == 1) and ( $r->get("id") != $this->id))
						{
							throw new EAV_Exception_Value_Duplicated();
						}
					}
					$this->set("modified", DB::expr("NOW()"));
				}
				if ($this->attribute->multiple == "yes")
				{
					if (is_array($value))
					{
						$values = $value;
					}
					else
					{
						$values[] = $value;
					}
					$value = 0;
				}
				else if ($this->attribute->multilanguage == "yes")
				{
					// HERE BE DRAGONS!
					$this->set("language", I18n::lang());
				}
				$this->set("value", $value);
				try
				{
					$this->save();
				} catch (ORM_Validation_Exception $e)
				{
					DataX::vardump($this->set->id);
					DataX::vardump($this->entity->entity_id);
					DataX::vardump($this->attribute->id);
					DataX::vardump($this->attribute->key);
					DataX::vardump($value);
					DataX::vardump($e->getCode());
					DataX::vardump($e->getMessage());
					DataX::vardump($e->getLine());
					DataX::vardump($this);
				}
				$this->reload();
				if ($this->attribute->searchable == "yes")
				{
					$search = new Model_EAV_Search($this->id);
					$search->write($this);
				}
				if (($this->attribute->multiple == "yes") and ( is_array($values)) and ( count($values) > 0))
				{
					// REWRTIE THIS AND USE MORE ORM
					switch ($this->attribute->type->name)
					{
						case "Entity":
							$model = "EAV_Entity_Attribute_Value_Entity_Value";
							$value_key = "eav_entity_attribute_value_entitiy_id";
							$option_key = "eav_entity_id";
							break;
						case "Multiple Select":
							$model = "EAV_Entity_Attribute_Value_Select_Value";
							$value_key = "eav_entity_attribute_value_select_id";
							$option_key = "eav_entity_attribute_option_id";
							break;
					}
					$obj = ORM::factory($model)->where($value_key, "=", $this->id)->find_all();
					foreach ($obj as $row)
					{
						$row->delete();
					}
					foreach ($values as $value)
					{
						$obj = ORM::factory($model);
						$obj->set($value_key, $this->id);
						$obj->set($option_key, $value);
						$obj->save();
					}
					// EOF REWRTIE THIS AND USE MORE ORM
				}
			}
		}
		return $this;
	}

}
