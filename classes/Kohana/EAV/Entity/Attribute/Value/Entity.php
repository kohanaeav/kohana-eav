<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Entity extends Kohana_EAV_Entity_Attribute_Value {
	
	protected $_table_name = "eav_entity_attribute_value_entity";
	protected $_value_model = "entity";

}
