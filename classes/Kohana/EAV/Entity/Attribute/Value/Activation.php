<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Activation extends Kohana_EAV_Entity_Attribute_Value {

	protected $_table_name = "eav_entity_attribute_value_activation";
	protected $_value_model = "line";

	public function set_value($value, $new = null)
	{
		if ( ! $this->loaded())
		{
			$obj = new $class($this->set, $this->entity, $this->attribute);
			$obj->set("eav_set_id", $this->set->id);
			$obj->set("eav_entity_id", $this->entity->entity_id);
			$obj->set("eav_attribute_id", $this->attribute->id);
			$obj->set("eav_attribute_key", $this->attribute->key);
			$obj->set("code", base64_encode(time() . serialize($_SERVER) . serialize($_SESSION)));
			$obj->set("value", $value);
			$obj->save();
			$obj->reload();
			return $obj;
		}
	}

}
