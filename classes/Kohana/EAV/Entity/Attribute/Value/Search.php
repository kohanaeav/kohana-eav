<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Search extends ORM {

	protected static $_search_table_name = "eav_entity_attribute_value_searchable";
	protected $_table_name = "eav_entity_attribute_value_searchable";

	public function __construct($id = NULL)
	{
		$this->_initialize();
		$this->where("eav_entity_attribute_value_entity_id", '=', $id)->find();
	}

	public function write($value)
	{
		if ( ! $this->loaded())
		{
			$this->set("model", $value->model());
			$this->set("eav_set_id", $value->eav_set_id);
			$this->set("eav_entity_id", $value->eav_entity_id);
			$this->set("eav_attribute_id", $value->eav_attribute_id);
			$this->set("eav_attribute_key", $value->eav_attribute_key);
			$this->set("eav_entity_attribute_value_entity_id", $value->id);
			if ($value->attribute->multilanguage == "yes")
			{
				$this->set("language", $value->language);
			}
		}
		$this->set("value", $value->value);
		$this->save();
	}

	public static function search($term, $set = null)
	{
		$objects = array();
		$sql_query = "SELECT * FROM " . self::$_search_table_name . " WHERE "
				. ( ! is_null($set) ? "eav_set_id = '{$set->id}' AND " : "")
				. "(MATCH (value) AGAINST ('{$term}') "
				. "OR value LIKE '%{$term}' "
				. "OR value LIKE '%{$term}%' "
				. "OR value LIKE '{$term}%') AND "
				. "(language IS NULL OR language = '" . i18n::lang() . "')";

		$q = DB::query(Database::SELECT, $sql_query);
		$result = $q->execute();
		foreach ($result as $row)
		{
			if (is_null($set))
			{
				$set = EAV::factory("Set", $row["eav_set_id"]);
			}
			$objects[$row["eav_entity_id"]] = EAV::factory($set, $row["eav_entity_id"]);
		}
		return $objects;
	}

}
