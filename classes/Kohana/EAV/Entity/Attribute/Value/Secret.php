<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Secret extends Kohana_EAV_Entity_Attribute_Value {

	protected $_table_name = "eav_entity_attribute_value_secret";
	protected $_value_model = "secret";

	public function get_value()
	{
		return null;
	}

	public function set_value($value, $new = false)
	{
		if ($value != "")
		{
			$class = get_class($this);
			if ($this->attribute->history == "yes" and ! $new)
			{
				$obj = new $class($this->set, $this->entity, $this->attribute);
				$obj->set("eav_set_id", $this->set->id);
				$obj->set("eav_entity_id", $this->entity->entity_id);
				$obj->set("eav_attribute_id", $this->attribute->id);
				$obj->set("eav_attribute_key", $this->attribute->key);
				$obj->set("value", $value);
				$obj->save();
				$obj->reload();
				$obj->set_value($value, true);
				return $obj;
			}
			else
			{
				if ( ! $this->loaded())
				{
					$this->set("eav_set_id", $this->set->id);
					$this->set("eav_entity_id", $this->entity->entity_id);
					$this->set("eav_attribute_id", $this->attribute->id);
					$this->set("eav_attribute_key", $this->attribute->key);
				}
				$this->set("value", DB::expr("PASSWORD('$value')"));
				$this->save();
				$this->reload();
			}
		}
		return $this;
	}

}
