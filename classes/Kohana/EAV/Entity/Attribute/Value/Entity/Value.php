<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Entity_Value extends ORM {
	
	protected $_table_name = "eav_entity_attribute_value_entity_values";
	
	protected $_belongs_to = array(
		"value" => array(
			"model" => "Model_EAV_Entity_Attribute_Value_Entity",
			"foreign_key" => "eav_entity_attribute_value_entitiy_id",
		),
		"entity" => array(
			"model" => "Model_EAV_Entity",
			"foreign_key" => "eav_entity_id",
		),
	);
}
