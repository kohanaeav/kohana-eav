<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Line extends Kohana_EAV_Entity_Attribute_Value {

	protected $_table_name = "eav_entity_attribute_value_line";
	protected $_value_model = "line";

	public function set_value($value, $new = false)
	{
		if ($this->attribute->key == "slug") {
			$set = EAV::factory("Set")->where("key", "=", strtolower($value))->find();
			if ($set->loaded()) {
				throw new EAV_Exception_Value_Forbidden();
			}
		}
		return parent::set_value($value, $new);
	}

}
