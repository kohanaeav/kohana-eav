<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model EAV_Core_Set_Attribute
 *
 * Created on 2014-04-08
 */
class Kohana_EAV_Entity_Attribute_Value_Picture extends Kohana_EAV_Entity_Attribute_Value {
	
	protected $_table_name = "eav_entity_attribute_value_file";
	protected $_value_model = "picture";
	
	public function get_value($size = NULL)
	{
		return $this->get("path")."/".$this->get("name");
	}

	public function set_value($value, $new = false)
	{

		return $this;
	}
	
}
