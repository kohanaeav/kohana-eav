<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Dummy EAV Class Extend if you need changes of EAV 
 *
 * @author Petar
 * Created on 2014-2-26
 */
class EAV extends Kohana_EAV { }
