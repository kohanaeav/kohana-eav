<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * ORM Model Form
 *
 * @author Petar
 * Created on 2014-3-20
 */
class EAV_Form extends Kohana_Form {

	public static function relation($entity, $type, $set, array $attributes = NULL)
	{
		switch ($type)
		{
			case 'hm_bt':
			case 'hm_ho':
//				$attributes["multiple"] = "multiple";
			case 'ho_bt':
			case 'bt_ho':
				$entities = EAV::factory($set->name)->find_all()->as_array("entity_id", "entity_name");
				$entities = array("0" => "Select") + $entities;
				return self::select($entity, $entities, "0", $attributes);
				break;
			case 'bt_hm':
			case 'ho_hm':
			case 'hm_hm':
				return self::entity($entity, $set, $attributes);
				break;
		}
	}

	public static function attribute($entity, $attribute, array $attributes = NULL)
	{
		$function = $attribute->type->name;
		if ($function == "Date / Time")
		{
			$function = "datetime";
		}
		if ($function == "Yes / No")
		{
			$function = "yesno";
		}
		if ($function == "Multiple Select")
		{
			$function = "select";
		}
		if (method_exists("EAV_Form", $function))
		{
			if (($function != "Entity") and ( $attribute->multiple != "yes") and ( $attribute->searchable != "yes"))
			{
				unset($attributes["autocomplete"]);
			}
			return self::$function($entity, $attribute, $attributes);
		}
		else
		{
			return parent::input(self::name($entity, $attribute), NULL, $attributes);
		}
	}

	protected static function name($entity, $attribute)
	{
		return "{$entity->set->key}_{$attribute->key}";
	}

	public static function picture($entity, $value_of, array $attributes = NULL)
	{
		try
		{
			$attribute_value = $entity->get($value_of->key);
			$value = $attribute_value->get_value();
		} catch (Kohana_Exception $e)
		{
			$value = "";
		}
		return parent::file(self::name($entity, $attribute), $attributes);
	}

	public static function line($entity, $value_of, array $attributes = NULL)
	{
		try
		{
			$attribute_value = $entity->get($value_of->key);
			$value = $attribute_value->get_value();
		} catch (Kohana_Exception $e)
		{
			$value = "";
		}
		return parent::input($value_of->key, $value, self::data_original($value, $attributes), $attributes);
	}

	public static function text($entity, $value_of, array $attributes = NULL)
	{
		try
		{
			$attribute_value = $entity->get($value_of->key);
			$value = $attribute_value->get_value();
		} catch (Kohana_Exception $e)
		{
			$value = "";
		}
		return parent::textarea($value_of->key, $value, self::data_original($value, $attributes), $attributes);
	}

	public static function datetime($entity, $value_of, array $attributes = NULL)
	{
		try
		{
			$attribute_value = $entity->get($value_of->key);
			$value = $attribute_value->get_value();
		} catch (Kohana_Exception $e)
		{
			$value = "";
		}
		return parent::input($value_of->key, $value, self::data_original($value, $attributes), $attributes);
	}

	public static function secret($entity, $value_of, array $attributes = NULL)
	{
		return parent::input($value_of->key, "", self::data_original("", $attributes), $attributes);
	}

	public static function number($entity, $value_of, array $attributes = NULL)
	{
		try
		{
			$attribute_value = $entity->get($value_of->key);
			$value = $attribute_value->get_value();
		} catch (Kohana_Exception $e)
		{
			$value = "";
		}
		return parent::input($value_of->key, $value, self::data_original($value, $attributes), $attributes);
	}

	public static function entity($entity, $relation, array $attributes = NULL)
	{

		$html = '<div class="input-group">' .
				'<span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>' .
				parent::input($relation["relation"]->name, "", self::data_original("", $attributes), $attributes) .
				'<span class="input-group-addon"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></span>' .
				'</div>';
		return $html;
	}

//
//	public static function entity($entity, $value_of, array $attributes = NULL)
//	{
//		if ($value_of->eav_set_id > 0)
//		{
//			$name = $value_of->key;
//			if ($value_of->multiple == "yes")
//			{
//				$name = $name . "[]";
//			}
//			$set = EAV::factory("Set", $value_of->eav_set_id);
//			$entitiyes = EAV::factory($set->key)->find_all()->as_array("entity_id", "entity_name");
//			Arr::unshift($entitiyes, 'null', __('Select'));
//			try
//			{
//				$attribute_value = $entity->get($value_of->key);
//				$value = $attribute_value->get_value();
//			} catch (Kohana_Exception $e)
//			{
//				$value = NULL;
//			}
//			if (($value_of->multiple == "yes") and ( is_null($value)))
//			{
//				$value = array();
//			}
//			return parent::select($name, $entitiyes, $value, $attributes);
//		}
//	}

	public static function select($name, array $options = NULL, $selected = NULL, array $attributes = NULL)
	{
		if ( ! isset($options['null']) && isset($attributes['default']) && $attributes['default'] == TRUE)
		{
			Arr::unshift($options, 'null', __('Select'));
		}

		return parent::select($name, $options, $selected, $attributes);
	}

	private static function data_original($value, array $attributes = NULL)
	{
		$original_value = array();
		if ( ! is_null($value))
		{
			$original_value = array("data_original" => $value);
		}
		if (is_array($attributes))
		{
			return array_merge($original_value, $attributes);
		}
		return $original_value;
	}

	private static function data_original_checkbox($checked, array $attributes = NULL)
	{

		return self::data_original(($checked ? "checked" : ""), $attributes);
	}

}

?>
