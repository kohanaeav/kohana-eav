<?php defined('SYSPATH') or die('No direct script access.'); 

/**
 * ORM Model EAV_Acounting_Set
 *
 * Created on 2014-04-08
 */

class Model_EAV_Slug extends Model_EAV_Entity_Attribute_Value_Line {
	public function __construct($slug = NULL)
	{
		$q = DB::select()->from($this->_table_name)
				->where("eav_attribute_key", "=", "slug")
				->and_where("value", "=", $slug);
		$r = $q->execute();
		if (count($r) > 0) {
			$d = $r[0];
			$set = EAV::factory("Set", $d["eav_set_id"]);
			$entity = EAV::factory("Entity", $d["eav_entity_id"]);
			$attribute = EAV::factory("Attribute", $d["eav_attribute_id"]);
			parent::__construct($set, $entity, $attribute, $d["id"]);
		}
	}
}
?>